# HTTP Client Options per URI

This module allows to set the options applied to outgoing Drupal requests
based on the URI of the request.

# Details

By default, Drupal uses Guzzle's HTTP Client for the outgoing HTTP requests
(for example, when Drupal needs to fetch data from a webservice). This HTTP
Client can be configured using `$settings` variable like this:

```
$settings['http_client_config']['timeout'] = 60;
```

That configuration would be applied to any outgoing connection using standard
Drupal HTTP request mechanism. However, sometimes different configurations are
needed for different outgoing connections. HTTP Client Options per URI solve
this problem allowing to define a set of options for different URIs.

# Use cases

This module is needed when Drupal needs to connect to different remote services
with different connections options instead of using the same configuration for
all outgoing connections.

For example, for performance reasons outgoing connection timeouts must be as
low as possible so that a slow remote web server doesn't cause Drupal requests
to take longer than recommended. However, some services are slower than others.
Using the same configuration for all services means using the highest timeout
that fits the slower service for all connections. Using this module a custom
timeout can be configured for the slower services, using a low timeout for the
other services.

# How it works

Guzzle uses the concept of [Middleware](https://docs.guzzlephp.org/en/stable/handlers-and-middleware.html#middleware)
to alter the requests and responses. This module replaces the  standard Drupal
`http_client_factory` service with a customized service that allows client
options per URI. To do this the module adds a custom Guzzle's Middleware
that checks `$settings['http_client_options_per_uri_config']` looking for
custom options on  outgoing HTTP connections.

# Usage

The module has no UI. To add a custom HTTP client options use the
`$settings['http_client_options_per_uri_config']` setting.

`$settings['http_client_options_per_uri_config']` is an array. Each key is an
 identifier for the custom options. Each custom option have the following
 entries:

 - regexp: a regular expression to determine to which outgoing connections
   the options will be applied. If the regexp matches, the options are applied.
   If more than one regexp matches only the last one is applied.

 - options: an array with [standard Guzzle options](https://docs.guzzlephp.org/en/stable/request-options.html) that will be applied to the outgoing connection.


# Examples

- To customize PayPal timeout.

```
$settings['http_client_options_per_uri_config']['paypal'] = [
  'regexp' => '/^http(?:s)?:\/\/api\.(?:sandbox\.)?paypal\.com.*$/i',
  'options' => [
    'timeout' => 10,
    'connect_timeout' => 10,
  ],
];
```

- To customize Mandrill timeout.

```
$settings['http_client_options_per_uri_config']['mandrill'] = [
  'regexp' => '/^http(?:s)?:\/\/mandrillapp\.com\/api\/.*$/i',
  'options' => [
    'timeout' => 10,
    'connect_timeout' => 10,
  ],
];
```
