<?php

namespace Drupal\http_client_options_per_uri\Service\Http;

use Drupal\Core\Site\Settings;
use Drupal\Core\Http\ClientFactory;
use Psr\Http\Message\RequestInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * HcopuClientFactory class to override options according to URI.
 *
 * 'Hcopu' stands for 'HTTP Client Options per URI'.
 */
class HcopuClientFactory extends ClientFactory {

  /**
   * {@inheritdoc}
   */
  public function fromOptions(array $config = []) {
    // Add middleware to modify options according to URI.
    $this->stack->unshift(HcopuClientFactory::requestOptionsMiddleware(), 'requestOptionsMiddleware');
    return parent::fromOptions($config);
  }

  /**
   * Middleware to modify options.
   *
   * @return mixed
   *   Middleware.
   */
  public static function requestOptionsMiddleware() {
    return function (callable $handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        self::requestOptionsAdjust($request, $options);
        return $handler($request, $options);
      };
    };
  }

  /**
   * Request options adjust.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request interface.
   * @param array $options
   *   Options.
   */
  public static function requestOptionsAdjust(RequestInterface $request, array &$options) {
    $uri_options = self::requestOptionsUriLocate((string) $request->getUri());
    if (!empty($uri_options)) {
      $options = NestedArray::mergeDeep($options, $uri_options);
    }
  }

  /**
   * Locate specific uri options to override (using regexp match).
   *
   * @param string $uri
   *   Request URI.
   *
   * @return array
   *   Result.
   */
  public static function requestOptionsUriLocate($uri) {
    $confs = Settings::get('http_client_options_per_uri_config', []);
    $result = [];
    foreach ($confs as $conf) {
      if (preg_match($conf['regexp'], $uri)) {
        $result = $conf['options'];
      }
    }
    return $result;
  }

}
